import pyvisa
from time import sleep
from ROOT import * 
from datetime import datetime,timedelta 
from statistics import mean
import signal,sys
import threading
import time

c = threading.Condition()
RUN = True
PL_FREQ = 60.


class T3DMM65 : 
    rm = 0
    inst = 0
    ip_address = ""
    meas_mode = ""
    trigg_mode = ""
    NPLC = 0
    t0 = datetime.now()
    sampleCount = 0

    def __init__(self,ip_address="192.168.1.220"):
        self.ip_address = ip_address
        self.rm = pyvisa.ResourceManager('@py')
        self.inst = self.rm.open_resource('TCPIP::%s::inst0::INSTR'%ip_address)
    
    
    def configure(self,mode="voltage"):
        if mode=="voltage":
            self.inst.write("CONF:VOLT:DC")
        elif mode == "current":
            self.inst.write("CONF:CURR:DC")
        elif mode == "resistance":
            self.inst.write("CONF:RES")
        elif mode == "temperature":
            self.inst.write("CONF:TEMP")
        else:
            print ("mode not available")
        self.meas_mode = mode
    
    def reset(self):
        self.inst.write("*RST")
    
    def trigger(self):
        self.inst.write("*TRG")
        self.t0=datetime.now()
        return self.t0

    def setSampleCount(self,count):
        self.inst.write("SAMP:COUN %i"%count)
        self.sampleCount=count


    def setTriggerCount(self,count):
        if(count == "INF"):
            self.inst.write("TRIG:COUN INF")
        else:
            self.inst.write("TRIG:COUN %i"%count)

    def setTrigger(self,mode="immediate"):

        if mode == "immediate":
            self.inst.write("TRIG:SOUR IMM")
        elif mode == "bus":
            self.inst.write("TRIG:SOUR BUS")
        elif mode == "external":
            self.inst.write("TRIG:SOUR EXT")
        else:
            print ("mode not available")
        self.trigg_mode=mode
    
    def ID(self):
        ID = self.inst.query("*IDN?")
        print (ID)
    
    def setWaitForTrigger(self):
        self.inst.write("INIT")
    
    def getLastData(self):
        value = self.inst.query("DATA:LAST?")
        print (value)
        #return float(value)
    
    def getMemoryData(self,npoints):
        # npoints = int(self.inst.query("DATA:POIN?"))
        # print (npoints)
        values = self.inst.query("R? %i"%npoints)
        data = [float(i) for i in values[5:].split(",")]
        return data

    def getMemoryDataCount(self):
        npoints = int(self.inst.query("DATA:POIN?"))
        return npoints        

    def setNPLC(self,NPLC):
        if(self.meas_mode == "voltage"):
            self.inst.write("VOLT:DC:NPLC %f"%NPLC)
        elif(self.meas_mode == "current"):
            self.inst.write("CURR:DC:NPLC %f"%NPLC)
        elif(self.meas_mode == "temperature" ):
            self.inst.write("TEMP:NPLC %f"%NPLC) 
        elif (self.meas_mode == "resistance") : 
            self.inst.write("RES:NPLC %f"%NPLC)
        self.NPLC = NPLC
    
    def close(self):
        print ("closing device")
        self.inst.close()
        self.rm.close()



def signal_handler(sig, frame):
    print('You pressed Ctrl+C!')
    #sys.exit(0)

def main_loop(can,gr,root_file,inst):
    nmeas = 10
    i=0
    root_file.cd()
    while RUN:
        sleep(0.1)
        c.acquire()
        while(inst.getMemoryDataCount()<nmeas):
            sleep(0.1)
        #average last measurements
        data = mean(inst.getMemoryData(nmeas))
        gr.SetPoint(i,inst.t0.timestamp(),data)
        inst.t0=inst.t0+inst.sampleCount*timedelta(microseconds=100000*(inst.sampleCount*1.0/PL_FREQ))
        can.Update()
        if(i%10==0):
            gr.Write("",TObject.kOverwrite)
        i=i+1

if __name__ == "__main__":

    
    #instantiate and configure T3DMM65
    inst = T3DMM65("192.168.1.220")
    inst.ID()
    inst.reset()
    inst.configure("voltage")
    inst.setSampleCount(10)
    inst.setNPLC(0.05)
    inst.setTrigger("immediate")
    inst.setTriggerCount("INF")
    
    #Arm device and send trigger
    inst.setWaitForTrigger()
    t0 = inst.trigger()


    signal.signal(signal.SIGINT,signal_handler)

    #prepare plotting
    can = TCanvas()
    gr = TGraph()

    gr.Draw("A*")
    gr.GetXaxis().SetTimeDisplay(1)
    gr.GetXaxis().SetNdivisions(5)
    gr.GetXaxis().SetTimeFormat("%Y-%m-%d %H:%M")
    gr.GetXaxis().SetTimeOffset(0,"local")
    gr.SetMarkerSize(0.3)
    gr.SetMarkerColor(kRed)

    root_file = TFile("DAC_TS_DATA.root","recreate")
    root_file.cd()
    gr.Write()
    #Measurement loop
    th = threading.Thread( target = main_loop, args=(can,gr,root_file,inst ) )
    th.start()
    signal.pause()
    RUN = False 
    th.join()
    
    inst.close()
    root_file.Close()    



